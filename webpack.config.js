const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  mode: 'development',
  // entry: path.resolve(__dirname, 'src/js/index.js'),
  entry: path.resolve(__dirname, 'src/ts/index.ts'),
  output: {
    filename: 'main.js',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  devServer: {
    open: true,
    // Следит за этими файлами и перегружает в страницу в браузере при их изменении
    watchFiles: ['src/**/*.html', 'src/**/**/*.scss', 'src/**/*.ts'],
    client: {
      logging: 'info',
      reconnect: true,
    },
    static: {
      directory: 'dist',
    },
    port: 3000,
    hot: true,
  },
  devtool: 'inline-source-map',
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'main.css',
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
    }),
    new ESLintPlugin({
      extensions: ['ts'],
      exclude: ['node_modules', 'dist', './webpack.config.js'],
      fix: true,
    }),
    new StylelintPlugin({
      fix: true,
      extensions: ['scss'],
      exclude: ['node_modules', 'dist'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader',
      },
      // {
      //   test: /\.(png|jpg|gif|svg)$/i,
      //   use: [
      //     {
      //       loader: 'url-loader',
      //       options: {
      //         limit: 8192,
      //       },
      //     },
      //   ],
      // },
      {
        test: /\.(png|jpe?g|gif|jp2|webp|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'public/images/[name].[ext]',
        },
      },
      // {
      //   test: /\.(png|jpe?g|gif|svg)$/i,
      //   loader: 'file-loader',
      //   options: {
      //     name: 'public/images/[name].[ext]',
      //   },
      //   type: 'javascript/auto',
      // },
      // {
      //   test: /\.(svg)$/i,
      //   use: [
      //     {
      //       loader: 'url-loader',
      //       options: {
      //         limit: 8192,
      //       },
      //     },
      //   ],
      // },
      // {
      //   test: /\.pug$/i,
      //   use: 'pug-loader',
      // },
    ],
  },
  optimization: {
    minimizer: [
      '...',
      new CssMinimizerPlugin(),
    ],
  },
};
