export class CommentBody {
  id: string;
  author: Author;
  replyID: string;
  time: string;
  text: string;
  likes: string[];
  dislikes: string[];
  constructor(id: string, reply: string, author: Author, text: string) {
    this.id = id;
    this.likes = [];
    this.dislikes = [];
    this.replyID = reply;
    this.author = author;
    const now = new Date();
    this.time = now.toString();
    this.text = text;
  }
  Likes(): number {
    return this.likes.length - this.dislikes.length
  }
  AutorInLikes(authorID: string): boolean {
    if (this.likes.includes(authorID)) {
      return true
    }
    return false
  }
  autorInDislikes(authorID: string): boolean {
    if (this.dislikes.includes(authorID)) {
      return true
    }
    return false
  }
  fromJSON(jsonA: string) {
    this.author = new Author('', '', '')
    this.author.fromJSON(JSON.stringify(JSON.parse(jsonA).author))

    this.id = JSON.parse(jsonA).id
    this.replyID = JSON.parse(jsonA).replyID
    this.time = JSON.parse(jsonA).time
    this.text = JSON.parse(jsonA).text
    if (JSON.parse(jsonA).likes) {
      this.likes = JSON.parse(jsonA).likes
    } else {
      this.likes = [];
    }
    if (JSON.parse(jsonA).dislikes) {
      this.dislikes = JSON.parse(jsonA).dislikes
    } else {
      this.dislikes = [];
    }
  }
}

export class Author {
  firstname: string;
  lastname: string;
  avatar: string;
  id: string;
  favorit: string[];
  constructor(firstname: string, lastname: string, id: string) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.id = id;
    this.avatar = `https://avatars.dicebear.com/api/bottts/${this.id}.svg`;
    this.favorit = [];
  }

  fromJSON(jsonA: string) {
    this.firstname = JSON.parse(jsonA).firstname;
    this.lastname = JSON.parse(jsonA).lastname;
    this.id = JSON.parse(jsonA).id;
    this.avatar = JSON.parse(jsonA).avatar;
    if (JSON.parse(jsonA).favorit) {
      this.favorit = JSON.parse(jsonA).favorit;
    } else {
      this.favorit = [];
    }
  }
}

