import { Author } from '../classes';
import {v4 as uuidv4} from 'uuid';

currentAuthor();

export function currentAuthor(): Author {
  const jsonA = window.localStorage.getItem('currentAuthor');
  let currentAuthor = new Author("", "", "");
  if  (jsonA) {
    currentAuthor.fromJSON(jsonA)
  }
  if (currentAuthor.firstname.length === 0) {
    currentAuthor = new Author("Главный", "Проверяющий 😎", uuidv4())
    window.localStorage.setItem('currentAuthor', JSON.stringify(currentAuthor));
  }
  return currentAuthor
}