
import { CommentBody } from '../classes';

export let comments: CommentBody[] = [];
const localComments = window.localStorage.getItem('allComments');
if (localComments) {  
  comments = JSON.parse(localComments)
} else {
  const c: CommentBody[] = [];
  window.localStorage.setItem('allComments', JSON.stringify(c));
}

export function getComments(): CommentBody[] {
  const c: CommentBody[] = [];
  const lc = window.localStorage.getItem('allComments');
  if (lc) {
    const badC:  CommentBody[] = JSON.parse(lc);
    badC.forEach( (com) => {
      const comm = new CommentBody('', '', com.author, '')
      comm.fromJSON(JSON.stringify(com))
      c.push(comm)
    });
  } 
  return c
}

export function getComment(id: string): CommentBody | null {
  let c: CommentBody[] = [];
  const lc = window.localStorage.getItem('allComments');
  if (lc) {
    c = JSON.parse(lc);
    const comment = c.find(element => element.id === id);
    if (comment) {
      const comm = new CommentBody('', '', comment.author, '')
      comm.fromJSON(JSON.stringify(comment))
      return comm
    }
  }
  return null;
}

export function AddComment(comment: CommentBody): void {
  let c: CommentBody[] = [];
  const lc = window.localStorage.getItem('allComments');
  if (lc) {
    c = JSON.parse(lc);
    c.push(comment);
    window.localStorage.setItem('allComments', JSON.stringify(c));
  }
}

export function UpdateComment(comment: CommentBody) {
  let c: CommentBody[] = [];
  const lc = window.localStorage.getItem('allComments');
  if (lc) {
    c = JSON.parse(lc);
    let ind = -1
    c.forEach((com, i) => {
      if (com.id === comment.id) {
        ind = i; 
      return;
      } 
    });
    if (ind !== -1) {
      c[ind] = comment;
    } else {
      c.push(comment);
    }
    window.localStorage.setItem('allComments', JSON.stringify(c));
  }
}


