import { CommentBody } from '../classes';
import {currentAuthor} from '../author/author';
import {v4 as uuidv4} from 'uuid';
import { AddComment, getComments, getComment, UpdateComment } from './comment';
import { GetSubComments, UpdateSubComment } from './subcomment';
import moment from 'moment';

function SubmitComment(identificator: string) {
  const commentHTML: HTMLElement | null = document.getElementById(identificator);
  if (commentHTML) {
    const commentInputValidation: HTMLInputElement | null = commentHTML.querySelector('.comments__all_text');
    if (commentInputValidation) {
      if (identificator === 'maininput') {
        const newComment = new CommentBody(uuidv4(), "", currentAuthor(), commentInputValidation.value)
        AddComment(newComment) 
        SetCommentsCount();
      } else {
        const newComment = new CommentBody(uuidv4(), "", currentAuthor(), commentInputValidation.value)
        UpdateSubComment(identificator, newComment)
      }
    }
  }
}

export function Render(comments: CommentBody[]): void {
  ClearComments();
  RenderComments(comments);
}

export function RenderInput(id: string) {
  let commentsRender: HTMLElement | null = null
  let identificator: string
  if (id === 'maininput') {
    commentsRender = document.querySelector('.comments__all');
    identificator = id;
  } else {
    const commentInput: HTMLElement | null = document.getElementById(`answer-${id}`);
    if (commentInput) {
      commentInput.remove();
      return
    }
    const commentHTML: HTMLElement | null = document.getElementById(id);
    if (commentHTML) {
      commentsRender = commentHTML.querySelector('.comment__wrapper');
    }
    identificator = `answer-${id}`
  }

  if (commentsRender) {
    const form = document.createElement('form');
    form.id = identificator; 
    form.action = `#${id}`
    const inputElement = `
    <div class="comments__all_input">
      <div class="comments__all_avatar"><img src="${currentAuthor().avatar}" alt="${currentAuthor().firstname} ${currentAuthor().lastname}"></div>
      <div class="comments__all_inputWrapper">
        <div class="comments__all_nameWrapper">
          <div class="comments__all_name">${currentAuthor().firstname} ${currentAuthor().lastname}</div>
          <div class="comments__all_validation"></div>
        </div>
        <textarea class="comments__all_text" name="mainText" rows="1" placeholder="Введите текст сообщения..." required form="${identificator}"></textarea>
      </div>
      <div class="comments__all_submitWrapper">
        <div class="comments__all_error">
          <span class="error__message">Слишком длинное сообщение</span>
        </div>
        <button class="comments__all_submit" type="submit" form="${identificator}">Отправить</button>
      </div>
    </div>`

    form.innerHTML = inputElement.trim();
    if (id === 'maininput') {
      commentsRender.appendChild(form);
    } else {
      commentsRender.insertBefore(form, commentsRender.children[commentsRender.children.length-1])
    }

    const commentForm: HTMLElement | null = document.getElementById(identificator);
    if (commentForm) {
      const validation: HTMLElement | null = commentForm.querySelector('.comments__all_validation');
      if (validation) {
        validation.innerHTML = "Макс. 1000 символов";
      }
      const commentInput: HTMLElement | null = commentForm.querySelector('.comments__all_text');
      if (commentInput) {
        commentInput.focus();
        commentInput.addEventListener('input', (event) => textareaInput(event, id));
      }
      const commentSubmit: HTMLButtonElement | null = commentForm.querySelector('.comments__all_submit');
      if (commentSubmit) {
        commentSubmit.addEventListener('input', (event) => textareaInput(event, id));
      }
      commentForm.onsubmit = () => SubmitComment(id);
    }

  }
}

export function SetCommentWidth(): void {
  const first: HTMLElement | null = document.querySelector('.comments__all_text');
  const second: HTMLElement | null = document.querySelector('.comments__all_submitWrapper');
  const text: HTMLElement | null = document.querySelector('.comment__text');
  if (first && second && text) {
    const w: number = first.offsetWidth + second.offsetWidth
    text.style.inlineSize = w + "px";
  }
}

export function SetCommentsCount(): void {
  const commentsCountNumber: HTMLInputElement | null = document.querySelector('.comments__count_number');
  if (commentsCountNumber) {
    commentsCountNumber.innerText = " (" + getComments().length + ")";
  }
}

function textareaInput(e: Event, id: string){
  let commentHTML: HTMLElement | null
  switch (true) {
  case id === 'maininput':
    commentHTML = document.getElementById(id);
    break;
  default:
    commentHTML = document.getElementById(`answer-${id}`);
    break;
  }
  if (commentHTML) {
    const commentError: HTMLElement | null = commentHTML.querySelector('.error__message');
    const validation: HTMLElement | null = commentHTML.querySelector('.comments__all_validation');
    const commentInput: HTMLElement | null = commentHTML.querySelector('.comments__all_text');
    const commentSubmit: HTMLButtonElement | null = commentHTML.querySelector('.comments__all_submit');

    setTimeout(function(){
      if (commentInput) {
        commentInput.style.cssText = 'height:auto; padding:0';
        commentInput.style.cssText = '-moz-box-sizing:content-box';
        commentInput.style.cssText = 'height:' + commentInput.scrollHeight + 'px';
      }
    },0);
    if (validation && commentSubmit) {
      switch (true) {
        case (e.target as HTMLInputElement).value.length === 0:
          if (commentError) {
            if (commentError.classList.contains("show")) {
              commentError.classList.remove("show");
            }
          }
          commentSubmit.disabled = false;
          if (validation.classList.contains("invalid")) {
            validation.classList.remove("invalid");
          }
          if (commentSubmit.classList.contains("active")) {
            commentSubmit.classList.remove("active");
          }
          validation.innerHTML = "Макс. 1000 символов";
          break;
        case (e.target as HTMLInputElement).value.length > 1000:
          if (commentError) {
            if (!commentError.classList.contains("show")) {
              commentError.classList.add("show");
            }
          }
          if (!validation.classList.contains("invalid")) {
            validation.classList.add("invalid");
          }
          commentSubmit.disabled = true;
          if (commentSubmit.classList.contains("active")) {
            commentSubmit.classList.remove("active");
          }
          validation.innerHTML = (e.target as HTMLInputElement).value.length + "/1000";
          break;
        default:
          if (commentError) {
            if (commentError.classList.contains("show")) {
              commentError.classList.remove("show");
            }
          }
          commentSubmit.disabled = false;
          if (!commentSubmit.classList.contains("active")) {
            commentSubmit.classList.add("active");
          }
          if (validation.classList.contains("invalid")) {
            validation.classList.remove("invalid");
          }
          validation.innerHTML = (e.target as HTMLInputElement).value.length + "/1000";
          break;
      }
    }
  }
}

function RenderComments(comments: CommentBody[]): void {
  const commentsDiv: HTMLElement | null = document.querySelector('.comments__all');
  const renderDiv = document.createElement("div");
  renderDiv.className = "comments__render"; 
  if (commentsDiv) {
    commentsDiv.appendChild(renderDiv);
  }
  // renderDiv.className = "comments__render1111555"; 
  comments.forEach((comment) => {
    const renderShow = document.createElement("div");
    const avatar = document.createElement("div");
    const avatarImg = document.createElement("img");
    const commentWrapper = document.createElement("div");
    const commentNameWrapper = document.createElement("div");
    const commentName = document.createElement("div");
    const commentTime = document.createElement("div");
    const commentText = document.createElement("p");
    const commentNav = document.createElement("nav");
    const commentUL = document.createElement("ul");
    const commentLiAnswer = document.createElement("li");
    const commentAnsIco = document.createElement("img");
    const commentAnsBut = document.createElement("button");
    const commentLiFavorite = document.createElement("li");
    const commentFavIco = document.createElement("img");
    const commentFavBtn = document.createElement("button");
    const commentLiLike = document.createElement("li");
    const commentMinus = document.createElement("div");
    const commentLinkeCount = document.createElement("span");
    const commentPlus = document.createElement("div");
    const subcomments = document.createElement("div");
    renderShow.className = 'comment__show';
    renderShow.id = comment.id;
    avatar.className = 'comment__avatar';
    avatarImg.src = comment.author.avatar;
    avatarImg.alt = comment.author.firstname;
    commentWrapper.className = 'comment__wrapper';
    commentNameWrapper.className = 'comment__nameWrapper';
    commentName.className = 'comment__name';
    commentName.innerText = comment.author.firstname + ' ' + comment.author.lastname
    commentTime.className = 'comment__time';
    const t = new Date(comment.time);
    commentTime.innerText = moment(t).format("DD.MM hh:mm:ss");
    commentText.className = 'comment__text';
    commentText.innerText = comment.text;
    commentNav.className = 'comment_navigation';
    commentUL.className = 'comment_navigation__list';
    commentLiAnswer.className = 'comment__answer';
    commentAnsIco.className = 'comment__answer_arrow';
    commentAnsBut.className = 'comment__answer_button';
    commentLiFavorite.className = 'comment__favorite';
    commentFavIco.className = 'comment__favorite_hart';
    commentFavBtn.className = 'comment__favorite_button';
    commentLiLike.className = 'comment__like';
    commentMinus.className = 'comment__like_minus';
    comment.autorInDislikes(currentAuthor().id) ? commentMinus.classList.add('hidden') : commentMinus.classList.remove('hidden');
    commentLinkeCount.className = 'comment__like_count';
    commentPlus.className = 'comment__like_plus';
    comment.AutorInLikes(currentAuthor().id) ? commentPlus.classList.add('hidden') : commentPlus.classList.remove('hidden');
    commentAnsIco.src = 'public/images/reply.svg';
    commentAnsBut.innerText = 'Ответить';
    commentFavIco.src = 'public/images/hart.svg';
    currentAuthor().favorit.includes(comment.id) ? commentFavBtn.innerText = 'В избранном' : commentFavBtn.innerText = 'В избранное';
    commentMinus.innerText = '-';
    commentLinkeCount.innerText = `${comment.Likes()}`;
    commentPlus.innerText = '+';

    
    renderDiv.appendChild(renderShow);
    avatar.appendChild(avatarImg);
    renderShow.appendChild(avatar);
    commentNameWrapper.appendChild(commentName);
    commentNameWrapper.appendChild(commentTime);
    commentWrapper.appendChild(commentNameWrapper);
    commentWrapper.appendChild(commentText);
    commentLiAnswer.appendChild(commentAnsIco);
    commentLiAnswer.appendChild(commentAnsBut);
    commentUL.appendChild(commentLiAnswer);
    commentLiFavorite.appendChild(commentFavIco);
    commentLiFavorite.appendChild(commentFavBtn);
    commentUL.appendChild(commentLiFavorite);
    commentLiLike.appendChild(commentMinus);
    commentLiLike.appendChild(commentLinkeCount);
    commentLiLike.appendChild(commentPlus);
    commentUL.appendChild(commentLiLike);
    commentNav.appendChild(commentUL)
    commentWrapper.appendChild(commentNav);
    const subs = GetSubComments(comment.id)
    subs.forEach((sub) => {
      const subRenderShow = document.createElement("div");
      const subAvatar = document.createElement("div");
      const subAvatarImg = document.createElement("img");
      const subCommentWrapper = document.createElement("div");
      const subCommentNameWrapper = document.createElement("div");
      const subCommentName = document.createElement("div");
      const subCommentReplyName = document.createElement("div");
      const subCommentAnswerArrow = document.createElement("img");
      const subCommentAnswerSpan = document.createElement("span");
      const subCommentTime = document.createElement("div");
      const subCommentText = document.createElement("p");
      subRenderShow.className = 'comment__show';
      subRenderShow.id = sub.id;
      subAvatar.className = 'comment__avatar';
      subAvatarImg.src = sub.author.avatar;
      subAvatarImg.alt = sub.author.firstname;
      subCommentWrapper.className = 'comment__wrapper';
      subCommentNameWrapper.className = 'comment__nameWrapper';
      subCommentName.className = 'comment__name';
      subCommentName.innerText = sub.author.firstname + ' ' + sub.author.lastname
      subCommentReplyName.className = 'comment__reply_name'
      subCommentAnswerArrow.className = 'comment__answer_arrow'
      subCommentAnswerArrow.src = 'public/images/reply.svg'
      subCommentAnswerSpan.className = 'comment__answer_button'
      subCommentAnswerSpan.innerText = comment.author.firstname
      subCommentTime.className = 'comment__time';
      const t = new Date(sub.time);
      subCommentTime.innerText = moment(t).format("DD.MM hh:mm:ss");
      subCommentText.className = 'comment__text';
      subCommentText.innerText = sub.text;

      subAvatar.appendChild(subAvatarImg);
      subRenderShow.appendChild(subAvatar);
      
      subCommentNameWrapper.appendChild(subCommentName);
      subCommentReplyName.appendChild(subCommentAnswerArrow)
      subCommentReplyName.appendChild(subCommentAnswerSpan)
      subCommentNameWrapper.appendChild(subCommentReplyName);
      subCommentNameWrapper.appendChild(subCommentTime);
      subCommentWrapper.appendChild(subCommentNameWrapper);
      subCommentWrapper.appendChild(subCommentText);
      subRenderShow.appendChild(subCommentWrapper);
      subcomments.appendChild(subRenderShow);
    });
    commentWrapper.appendChild(subcomments);
    renderShow.appendChild(commentWrapper);
    
    
    commentAnsIco.addEventListener('click', () => RenderInput(comment.id));
    commentAnsBut.addEventListener('click', () => RenderInput(comment.id));
    commentFavIco.addEventListener('click', commentFavoritF);
    commentFavBtn.addEventListener('click', commentFavoritF);
    commentMinus.addEventListener('click', commentMinusF);
    commentPlus.addEventListener('click', commentPlusF);
    renderFavorite(comment.id);
  });
}

function ClearComments(): void {
  const comments: HTMLElement | null = document.querySelector('.comments__render');
  if (comments) {
    comments.remove();
  }
}

function commentFavoritF(e: Event): void {
  const curAuth = currentAuthor()
  const id: string = getCurrentCommentID(e);
  if (curAuth.favorit.includes(id)) {
    for( let i = 0; i < curAuth.favorit.length; i++){ 
      if ( curAuth.favorit[i] === id) { 
        curAuth.favorit.splice(i, 1); 
        break;
      }
    }
  } else {
    curAuth.favorit.push(id)
  }
  window.localStorage.setItem('currentAuthor', JSON.stringify(curAuth));
  renderFavorite(id);
}

function commentMinusF(e: Event): void {
  const id: string = getCurrentCommentID(e);
  const comment = getComment(id)

  if (comment) {
    switch (true) {
      case comment.autorInDislikes(currentAuthor().id):
        break;
      case comment.AutorInLikes(currentAuthor().id):
        comment.likes = comment.likes.filter(id => id !== currentAuthor().id);
        break;
      default:
        comment.dislikes.push(currentAuthor().id)
        break;
    }

    UpdateComment(comment)
    renderCommentLikes(id, comment.Likes());
  }
}

function commentPlusF(e: Event): void {
  const id: string = getCurrentCommentID(e);
  const comment = getComment(id)
  if (comment) {
    switch (true) {
      case comment.AutorInLikes(currentAuthor().id):
        break;
      case comment.autorInDislikes(currentAuthor().id):
        comment.dislikes = comment.dislikes.filter(id => id !== currentAuthor().id);
        break;
      default:
        comment.likes.push(currentAuthor().id)
        break;
    }
    UpdateComment(comment)
    renderCommentLikes(id, comment.Likes());
  }
}

function getCurrentCommentID(e: Event): string {
  const target: HTMLElement | null = e.target as HTMLElement;
  const commentShow = target.closest('.comment__show');
  if (commentShow) {
    return commentShow.id
  }
  return ''
}

function renderFavorite(id: string): void {
  const commentHTML: HTMLElement | null = document.getElementById(id);
  if (commentHTML) {
    const hart: HTMLImageElement | null = commentHTML.querySelector('.comment__favorite_hart');
    if (hart) {
      if (currentAuthor().favorit.includes(id)) {
        hart.classList.add('filled') 
        hart.src = 'public/images/hart_filled.svg'
      } else {
        hart.classList.remove('filled')
        hart.src = 'public/images/hart.svg'
      }
    }
    const hartText: HTMLImageElement | null = commentHTML.querySelector('.comment__favorite_button');
    if (hartText) {
      if (currentAuthor().favorit.includes(id)) {
        hartText.textContent = 'В избранном';
      } else {
        hartText.textContent = 'В избранное';
      }
    }
  }
}

function renderCommentLikes(id: string, likes: number): void {
  const commentHTML: HTMLElement | null = document.getElementById(id);
  if (commentHTML) {
    const like: HTMLElement | null = commentHTML.querySelector('.comment__like_count');
    if (like) {
      like.innerText = `${likes}`
    }
    const comment = getComment(id)
    if (comment) {
      const minus: HTMLElement | null = commentHTML.querySelector('.comment__like_minus');
      if (minus) {
        comment.autorInDislikes(currentAuthor().id) ? minus.classList.add('hidden') : minus.classList.remove('hidden');
      }
      const plus: HTMLElement | null = commentHTML.querySelector('.comment__like_plus');
      if (plus) {
        comment.AutorInLikes(currentAuthor().id) ? plus.classList.add('hidden') : plus.classList.remove('hidden');
      }
    }
  }
}

export function FilteredComments(): CommentBody[] {
  let comments: CommentBody[] = getComments();
  const fav = window.localStorage.getItem('ShowFavorite');
  const filt = window.localStorage.getItem('CurrentFilter');
  if (fav) {
    if (fav === 'true') {
      comments = getComments().filter(x => currentAuthor().favorit.includes(x.id));
    }
  }
  if (filt) {
    switch (filt) {
      case "По дате":
        comments.sort((a,b)=> {
          const dateOne = new Date(a.time);
          const dateTwo = new Date(b.time);
          return dateOne.getTime() - dateTwo.getTime();
        });
        break;
      case "По количеству оценок":
        comments.sort((a,b)=> {
          if ((b.likes.length + b.dislikes.length) - (a.likes.length + a.dislikes.length) === 0) {
            const dateOne = new Date(a.time);
            const dateTwo = new Date(b.time);
            return dateOne.getTime() - dateTwo.getTime();
          } else {
            return (b.likes.length + b.dislikes.length) - (a.likes.length + a.dislikes.length);
          }
        });
        break;
        case "По актуальности":
          comments.sort((a,b)=> {
            if (b.Likes() - a.Likes() === 0) {
              const dateOne = new Date(a.time);
              const dateTwo = new Date(b.time);
              return dateOne.getTime() - dateTwo.getTime();
            } else {
              return b.Likes() - a.Likes();
            }
          });
        break;
      case "По количеству ответов":
        comments.sort((a,b)=> {
          if (GetSubComments(b.id).length - GetSubComments(a.id).length === 0) {
            const dateOne = new Date(a.time);
            const dateTwo = new Date(b.time);
            return dateOne.getTime() - dateTwo.getTime();
          } else {
            return GetSubComments(b.id).length - GetSubComments(a.id).length;
          }
        });
        break;
    }
  }
  return comments
}
