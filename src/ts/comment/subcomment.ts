import { CommentBody } from '../classes';

const localSubComments = window.localStorage.getItem('allSubComments');
if (!localSubComments) {
  const subcomments = new Map<string, CommentBody[]>();
  window.localStorage.setItem('allSubComments', JSON.stringify(Array.from(subcomments.entries())));
}

export function getAll(): Map<string, CommentBody[]> {
  const subcomments = new Map<string, CommentBody[]>();

  const lc = window.localStorage.getItem('allSubComments');
  if (lc) {
    const sub = new Map<string, CommentBody[]>(JSON.parse(lc));
    if (sub.size) {
      sub.forEach((v,k) => {
        const c: CommentBody[] = [];
        v.forEach((com) => {
          const comm = new CommentBody('', '', com.author, '')
          comm.fromJSON(JSON.stringify(com))
          c.push(comm)
        });
        subcomments.set(k, c);
      });
    }
  } 
  return subcomments
}

export function GetSubComments(id: string): CommentBody[] {
  const allSubComments: Map<string, CommentBody[]> = getAll();
  if (allSubComments) {
    const subcomments = allSubComments.get(id);
    if (subcomments !== undefined) {
      return subcomments
    }
  }
  return [];
}

export function UpdateSubComment(id: string, comment: CommentBody) {
  const allSubComments: Map<string, CommentBody[]> = getAll();
  const subs = GetSubComments(id);
  subs.push(comment);
  allSubComments.set(id, subs)
  window.localStorage.setItem('allSubComments', JSON.stringify(Array.from(allSubComments.entries())));
}