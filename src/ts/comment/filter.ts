import { Render, FilteredComments } from './create';

export function SetFilters(): void {
  
  const fav = window.localStorage.getItem('ShowFavorite');
  if (!fav) {
    window.localStorage.setItem('ShowFavorite', 'false');
  }
  
  let filt = window.localStorage.getItem('CurrentFilter');
  if (!filt) {
    window.localStorage.setItem('CurrentFilter', 'По дате');
    filt = window.localStorage.getItem('CurrentFilter');
  }
  
  const filter : HTMLElement | null = document.querySelector('.comments__filter');
  if (filter) {
    const filterBtn: HTMLElement | null = filter.querySelector('.comments__filter_button');
    const filterSelect: HTMLElement | null = filter.querySelector('.comments__filter_select');
    const filterArrow: HTMLElement | null = filter.querySelector('.comments__filter_arrow');
    if (filterBtn) {
      if (filt) {
        filterBtn.innerText = filt; 
      }
      filterBtn.addEventListener('click', function() {
        if (filterSelect) {
          if (filterSelect.classList.contains("hidden")) {
            filterSelect.classList.remove("hidden");
          } else {
            filterSelect.classList.add("hidden");
          }
        }
        if (filterArrow) {
          if (filterArrow.classList.contains("open")) {
            filterArrow.classList.remove("open");
          } else {
            filterArrow.classList.add("open");
          }
        }
      });
    }
    if (filterArrow) {
      filterArrow.addEventListener('click', function() {
        if (filterSelect) {
          if (filterSelect.classList.contains("hidden")) {
            filterSelect.classList.remove("hidden");
          } else {
            filterSelect.classList.add("hidden");
          }
        }
        if (filterArrow) {
          if (filterArrow.classList.contains("open")) {
            filterArrow.classList.remove("open");
          } else {
            filterArrow.classList.add("open");
          }
        }
      });
    }
  
    const filterRadios = document.querySelectorAll('.radio__wrapper');
    
    filterRadios.forEach(radio => {
      if ((radio.firstElementChild as HTMLInputElement).value === filt) {
        (radio.firstElementChild as HTMLInputElement).checked = true
      }
      radio.addEventListener('change', function(e){
        window.localStorage.setItem('CurrentFilter', (e.target as HTMLInputElement).value);
        if (filterBtn) {
          filterBtn.innerText = (e.target as HTMLInputElement).value;
        }
        if (filterSelect) {
          if (filterSelect.classList.contains("hidden")) {
            filterSelect.classList.remove("hidden");
          } else {
            filterSelect.classList.add("hidden");
            
          }
        }
        if (filterArrow) {
          if (filterArrow.classList.contains("open")) {
            filterArrow.classList.remove("open");
          } else {
            filterArrow.classList.add("open");
          }
        }
        Render(FilteredComments());
      });
    });
  }
  
  const favorites : HTMLElement | null = document.querySelector('.comments__favorite');
  if (favorites) {
    const filterBtn: HTMLElement | null = favorites.querySelector('.comments__filter_button');
    const filterArr: HTMLElement | null = favorites.querySelector('.comments__filter_arrow');
  
    if (filterBtn) {
      filterBtn.addEventListener('click', function() {
        const fav = window.localStorage.getItem('ShowFavorite');
        if (fav) {
          if (fav === 'true') {
            window.localStorage.setItem('ShowFavorite', 'false');
          } else {
            window.localStorage.setItem('ShowFavorite', 'true');
          }
        } else {
          window.localStorage.setItem('ShowFavorite', 'false');
        }
        Render(FilteredComments());
      });
    }
    if (filterArr) {
      filterArr.addEventListener('click', function() {
        const fav = window.localStorage.getItem('ShowFavorite');
        if (fav) {
          if (fav === 'true') {
            window.localStorage.setItem('ShowFavorite', 'false');
          } else {
            window.localStorage.setItem('ShowFavorite', 'true');
          }
        } else {
          window.localStorage.setItem('ShowFavorite', 'false');
        }
        Render(FilteredComments());
      });
    }
  }
}