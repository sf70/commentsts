import { Render, FilteredComments, RenderInput, SetCommentsCount, SetCommentWidth } from './create';
import { SetFilters } from './filter';
import { Mocks } from './mock';


Mocks();
SetFilters();
RenderInput('maininput');
Render(FilteredComments());
SetCommentsCount();
SetCommentWidth();

