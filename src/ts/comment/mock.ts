import { CommentBody } from '../classes';

export function Mocks(): void {

  const comments = `[
    {
      "id": "2fd7dc21-d4a0-44b3-a192-7b3b78d4858f",
      "likes": [],
      "dislikes": [
        "0fa85bbc-8e98-4d35-8a8b-7b65e9c668ae",
        "fd07c021-0748-4ccd-a54a-1972fa7fc79d",
        "e100441f-1dd6-4de7-ab86-79752c28ef12",
        "28a5ddb4-2418-461f-a4f8-22251d3a49ec",
        "91a5330a-07b8-4f68-958e-6f0dc1fc66c6"
      ],
      "replyID": "",
      "author": {
        "firstname": "Сашка",
        "lastname": "Белый",
        "id": "7f5849a9-53f6-4a7e-99f3-3ee78cf6ec9e",
        "avatar": "https://avatars.dicebear.com/api/bottts/7f5849a9-53f6-4a7e-99f3-3ee78cf6ec9e.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:07:48 GMT+0300 (Moscow Standard Time)",
      "text": "Комментарий №1 Самый первый."
    },
    {
      "id": "8df081bf-a8f1-4258-b07c-2d2db430d2b6",
      "likes": [
        "0fa85bbc-8e98-4d35-8a8b-7b65e9c668ae",
        "fd07c021-0748-4ccd-a54a-1972fa7fc79d",
        "e100441f-1dd6-4de7-ab86-79752c28ef12"
      ],
      "dislikes": [],
      "replyID": "",
      "author": {
        "firstname": "Максим",
        "lastname": "Авдеенко",
        "id": "0fa85bbc-8e98-4d35-8a8b-7b65e9c668ae",
        "avatar": "https://avatars.dicebear.com/api/bottts/0fa85bbc-8e98-4d35-8a8b-7b65e9c668ae.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:08:04 GMT+0300 (Moscow Standard Time)",
      "text": "Комментарий №2 Самый второй."
    },
    {
      "id": "b4c500f3-13a3-46b8-ac21-8cd24408983a",
      "likes": [
        "e100441f-1dd6-4de7-ab86-79752c28ef12"
      ],
      "dislikes": [],
      "replyID": "",
      "author": {
        "firstname": "Виталя",
        "lastname": "MR.Presedent",
        "id": "fd07c021-0748-4ccd-a54a-1972fa7fc79d",
        "avatar": "https://avatars.dicebear.com/api/bottts/fd07c021-0748-4ccd-a54a-1972fa7fc79d.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:08:47 GMT+0300 (Moscow Standard Time)",
      "text": "Весело тут у вас 😅"
    },
    {
      "id": "3d6f53d7-1d12-41d8-8bf6-9bbf467c49f7",
      "likes": [],
      "dislikes": [],
      "replyID": "",
      "author": {
        "firstname": "Илон",
        "lastname": "Маск",
        "id": "e100441f-1dd6-4de7-ab86-79752c28ef12",
        "avatar": "https://avatars.dicebear.com/api/bottts/e100441f-1dd6-4de7-ab86-79752c28ef12.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:09:22 GMT+0300 (Moscow Standard Time)",
      "text": "А фильтры работают?"
    },
    {
      "id": "d1fd2e27-56ed-4c04-b822-9e7729244b7a",
      "likes": [
        "80813ab4-fdea-4cb5-ae2f-5f0ff5150e87",
        "54016fbe-18c7-420a-a30e-6313efc774d7",
        "13cd9030-2fbe-4762-a1e4-7efd82c3014f",
        "28a5ddb4-2418-461f-a4f8-22251d3a49ec",
        "60232ad3-051f-4572-91e6-1ad25ac8ea49",
        "6c7c8467-b2e1-4597-932a-f5f6d8b6704e",
        "b7656287-6d88-4b48-892e-ae7f9903560b",
        "91a5330a-07b8-4f68-958e-6f0dc1fc66c6"
      ],
      "dislikes": [],
      "replyID": "",
      "author": {
        "firstname": "Юлия",
        "lastname": "Новикова",
        "id": "80813ab4-fdea-4cb5-ae2f-5f0ff5150e87",
        "avatar": "https://avatars.dicebear.com/api/bottts/80813ab4-fdea-4cb5-ae2f-5f0ff5150e87.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:12:57 GMT+0300 (Moscow Standard Time)",
      "text": "Крутая системка)) надо попросить чтоб 10 из 6и поставили пареньку 🤓 Кто за?"
    },
    {
      "id": "c5bea3f5-f8d1-460d-bd76-49e776bcbb2a",
      "likes": [],
      "dislikes": [],
      "replyID": "",
      "author": {
        "firstname": "Татьяна",
        "lastname": "Чанышева",
        "id": "114241e2-e83b-4456-968f-dd5f5def17c6",
        "avatar": "https://avatars.dicebear.com/api/bottts/114241e2-e83b-4456-968f-dd5f5def17c6.svg",
        "favorit": []
      },
      "time": "Sun Jan 15 2023 22:14:34 GMT+0300 (Moscow Standard Time)",
      "text": "Нра"
    }
  ]`
  const subs = `[
  [
    "3d6f53d7-1d12-41d8-8bf6-9bbf467c49f7",
    [
      {
        "id": "befda93d-afc2-4dc8-a9d3-c6626ae70012",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Борис",
          "lastname": "ХренпопадЁшь",
          "id": "8198e382-90b2-4466-a0e8-cd8650b2f54e",
          "avatar": "https://avatars.dicebear.com/api/bottts/8198e382-90b2-4466-a0e8-cd8650b2f54e.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:11:10 GMT+0300 (Moscow Standard Time)",
        "text": "А ты попробуй. Гланое помни что количество оценок это общее количество, рпложительных и отрицательных, а актуальность это рейтинг с учетом положительности и отриательности оценки))"
      },
      {
        "id": "cddd734f-7b3e-47e3-84ab-7cb01cfefc7f",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Юлия",
          "lastname": "Новикова",
          "id": "80813ab4-fdea-4cb5-ae2f-5f0ff5150e87",
          "avatar": "https://avatars.dicebear.com/api/bottts/80813ab4-fdea-4cb5-ae2f-5f0ff5150e87.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:11:24 GMT+0300 (Moscow Standard Time)",
        "text": "это правда)))"
      }
    ]
  ],
  [
    "2fd7dc21-d4a0-44b3-a192-7b3b78d4858f",
    [
      {
        "id": "d550d75d-1167-472f-b901-01221f41cc7c",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Юлия",
          "lastname": "Новикова",
          "id": "80813ab4-fdea-4cb5-ae2f-5f0ff5150e87",
          "avatar": "https://avatars.dicebear.com/api/bottts/80813ab4-fdea-4cb5-ae2f-5f0ff5150e87.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:11:53 GMT+0300 (Moscow Standard Time)",
        "text": "Ну ты прям намбер ВАН))"
      }
    ]
  ],
  [
    "d1fd2e27-56ed-4c04-b822-9e7729244b7a",
    [
      {
        "id": "0e607118-7773-4074-932f-dfc04f655dff",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Пика",
          "lastname": "Чу",
          "id": "09df6d36-8b07-4079-ad21-f5d66ec08f84",
          "avatar": "https://avatars.dicebear.com/api/bottts/09df6d36-8b07-4079-ad21-f5d66ec08f84.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:13:14 GMT+0300 (Moscow Standard Time)",
        "text": "+"
      },
      {
        "id": "9273e3c1-b80e-4f80-849c-5569fecf4765",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Винни",
          "lastname": "Пух",
          "id": "54016fbe-18c7-420a-a30e-6313efc774d7",
          "avatar": "https://avatars.dicebear.com/api/bottts/54016fbe-18c7-420a-a30e-6313efc774d7.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:13:26 GMT+0300 (Moscow Standard Time)",
        "text": "Еще + ))"
      },
      {
        "id": "15855672-d93e-4c32-b0ce-b79ec97de3f5",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "James",
          "lastname": "Bond",
          "id": "91a5330a-07b8-4f68-958e-6f0dc1fc66c6",
          "avatar": "https://avatars.dicebear.com/api/bottts/91a5330a-07b8-4f68-958e-6f0dc1fc66c6.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:14:11 GMT+0300 (Moscow Standard Time)",
        "text": "Единогласно))"
      }
    ]
  ],
  [
    "c5bea3f5-f8d1-460d-bd76-49e776bcbb2a",
    [
      {
        "id": "dd33f5c7-0c7c-4626-9ecb-194bc145065a",
        "likes": [],
        "dislikes": [],
        "replyID": "",
        "author": {
          "firstname": "Душный",
          "lastname": "Душнила",
          "id": "bfb864b9-a82a-4628-b3ae-9c9d06a51be3",
          "avatar": "https://avatars.dicebear.com/api/bottts/bfb864b9-a82a-4628-b3ae-9c9d06a51be3.svg",
          "favorit": []
        },
        "time": "Sun Jan 15 2023 22:14:59 GMT+0300 (Moscow Standard Time)",
        "text": "???)"
      }
    ]
  ]
]`

  const com = window.localStorage.getItem('allComments');
  if (com) {
    const badC:  CommentBody[] = JSON.parse(com);
    if (badC.length === 0) {
      window.localStorage.setItem('allComments', comments);
    }
  }
  const sub = window.localStorage.getItem('allSubComments');


  if (sub) {
    const badC:  CommentBody[] = JSON.parse(sub);
    if (badC.length === 0) {
      window.localStorage.setItem('allSubComments', subs);
    }
  }
}






